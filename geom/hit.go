package geom

//record of a ray r hitting a sphere at point p = r(T), with normal facing away from the surface
type HitRecord struct {
	T      float64
	P      Vec
	Normal Vec
	Mat    Material
}

type Hittable interface {
	Hit(r Ray, tMin float64, tMax float64, rec *HitRecord) bool
}

type List struct {
	List []Hittable
}

func (l List) Hit(r Ray, tMin float64, tMax float64, rec *HitRecord) bool {
	var tRec HitRecord
	var hitRecorded bool = false
	closestSoFar := tMax
	for _, v := range l.List {
		if v.Hit(r, tMin, closestSoFar, &tRec) {
			hitRecorded = true
			closestSoFar = tRec.T
			(*rec) = tRec
		}
	}
	return hitRecorded
}
