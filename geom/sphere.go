package geom

import (
	"math"
)

type Sphere struct {
	Center Vec
	Radius float64
	Mat    Material
}

//Return true if ray r hits sphere s
func (s Sphere) Hit(r Ray, tMin float64, tMax float64, rec *HitRecord) bool {
	oc := r.Origin.Minus(s.Center)
	a := r.Direction.Dot(r.Direction)
	b := 2 * oc.Dot(r.Direction)
	c := oc.Dot(oc) - s.Radius*s.Radius
	disc := b*b - 4*a*c
	if disc > 0 {
		t := (-b - math.Sqrt(disc)) / (2 * a)
		if t < tMax && t > tMin {
			rec.T = t
			rec.P = r.pointAt(t)
			rec.Normal = (rec.P.Minus(s.Center)).Scaled(1.0 / s.Radius) // normal as unit vector
			rec.Mat = s.Mat
			return true
		}
		t = (-b + math.Sqrt(disc)) / (2 * a)
		if t < tMax && t > tMin {
			rec.T = t
			rec.P = r.pointAt(t)
			rec.Normal = (rec.P.Minus(s.Center)).Scaled(1.0 / s.Radius) // normal as unit vector
			rec.Mat = s.Mat
			return true
		}
	}
	return false
}
