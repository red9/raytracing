package geom

import (
	"gitlab.com/red9/raytracing/rnd"
	"math"
)

type Material interface {
	Scatter(ray_in *Ray, rec *HitRecord, attenuation *Vec, ray_out *Ray, rand *rnd.RandSrc) bool
}

type Matte struct {
	Albedo Vec
}

func (m Matte) Scatter(rayIn *Ray, rec *HitRecord, attenuation *Vec, rayOut *Ray, rand *rnd.RandSrc) bool {
	*rayOut = Ray{rec.P, rec.Normal.Plus(Random(rand))}
	*attenuation = m.Albedo
	return true
}

type Metal struct {
	Albedo Vec
	Fuzz   float64
}

func (m Metal) Scatter(rayIn *Ray, rec *HitRecord, attenuation *Vec, rayOut *Ray, rand *rnd.RandSrc) bool {
	refl := reflect(rayIn.Direction, rec.Normal)
	*rayOut = Ray{rec.P, refl.Plus(Random(rand).Scaled(m.Fuzz))}
	*attenuation = m.Albedo
	return (rayOut.Direction.Dot(rec.Normal) > 0)
}

func reflect(v Vec, n Vec) Vec {
	return v.Minus(n.Scaled(2 * v.Dot(n)))
}

type Dielectric struct {
	Albedo   Vec
	RefIndex float64
}

func (m Dielectric) Scatter(rayIn *Ray, rec *HitRecord, attenuation *Vec, rayOut *Ray, rand *rnd.RandSrc) bool {
	var outwardNormal Vec
	reflected := reflect(rayIn.Direction, rec.Normal)
	var refracted Vec
	var ni_nt float64
	*attenuation = m.Albedo
	var cos float64
	if rayIn.Direction.Dot(rec.Normal) > 0 {
		outwardNormal = rec.Normal.Scaled(-1)
		ni_nt = m.RefIndex
		cos = m.RefIndex * rayIn.Direction.Dot(rec.Normal) / rayIn.Direction.Len()
	} else {
		outwardNormal = rec.Normal
		ni_nt = 1.0 / m.RefIndex
		cos = rayIn.Direction.Dot(rec.Normal) * -1 / rayIn.Direction.Len()
	}
	if refract(rayIn.Direction, outwardNormal, ni_nt, &refracted) {
		refProb := schlick(cos, m.RefIndex)
		if rand.Next() < refProb {
			*rayOut = Ray{rec.P, reflected}
		} else {
			*rayOut = Ray{rec.P, refracted}
		}
	} else {
		*rayOut = Ray{rec.P, reflected}
	}
	return true
}

func refract(v Vec, n Vec, ni_nt float64, refracted *Vec) bool {
	uv := v.ToUnit()
	dt := uv.Dot(n)
	disc := 1.0 - ni_nt*ni_nt*(1-dt*dt)
	if disc > 0 {
		*refracted = uv.Minus(n.Scaled(dt)).Scaled(ni_nt).Minus(n.Scaled(math.Sqrt(disc)))
		return true
	} else {
		return false
	}
}

func schlick(cos float64, RefIndex float64) float64 {
	r0 := (1 - RefIndex) / (1 + RefIndex)
	r0 = r0 * r0
	return r0 + (1-r0)*math.Pow((1-cos), 5)

}
