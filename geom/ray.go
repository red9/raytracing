package geom

// Ray represents a ray starting at a point going in a direction
type Ray struct {
	Origin    Vec
	Direction Vec
}

//Returns a point on the ray
func (r Ray) pointAt(t float64) Vec {
	return r.Origin.Plus(r.Direction.Scaled(t))
}
