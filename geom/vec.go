package geom

import (
	"fmt"
	"gitlab.com/red9/raytracing/rnd"
	"io"
	"math"
)

//Vec represents a 3-element vector
type Vec struct {
	E [3]float64
}

func NewVec(e0, e1, e2 float64) Vec {
	return Vec{E: [3]float64{e0, e1, e2}}
}

func Random(rnd *rnd.RandSrc) Vec {
	var p Vec
	//do while point p is not in unit sphere
	for ok := true; ok; ok = (p.LenSq() >= 1) {
		p = NewVec(rnd.Next(), rnd.Next(), rnd.Next()).Scaled(2).Minus(NewVec(1, 1, 1))
	}
	return p
}

func RandomInUnitCircle(vertical Vec, horizontal Vec, rnd *rnd.RandSrc) Vec {
	var p Vec
	//do while point p is not in unit circle
	for ok := true; ok; ok = (p.LenSq() >= 1) {
		v := vertical.Scaled(rnd.Next()*2 - 1)
		h := horizontal.Scaled(rnd.Next()*2 - 1)
		p = v.Plus(h)
	}
	return p
}

// X returns the first element
func (v Vec) X() float64 {
	return v.E[0]
}

// Y returns the second element
func (v Vec) Y() float64 {
	return v.E[1]
}

// Z returns the third element
func (v Vec) Z() float64 {
	return v.E[2]
}

// Aliases when using the vector class to denote colours

// R returns the first element (Red)
func (c Vec) R() float64 {
	return c.E[0]
}

// G returns the second element (Green)
func (c Vec) G() float64 {
	return c.E[1]
}

// B returns the third element (Blue)
func (c Vec) B() float64 {
	return c.E[2]
}

func (v Vec) Inv() Vec {
	return NewVec(-v.E[0], -v.E[1], -v.E[2])
}

func (v Vec) Len() float64 {
	return math.Sqrt(v.LenSq())
}

func (v Vec) LenSq() float64 {
	return v.E[0]*v.E[0] + v.E[1]*v.E[1] + v.E[2]*v.E[2]
}

// ToUnit converts this vector to a unit vector
func (v Vec) ToUnit() Vec {
	k := 1.0 / v.Len()
	return v.Scaled(k)
}

// IStream streams in space-separated vector values from a Reader
func (v Vec) IStream(r io.Reader) error {
	_, err := fmt.Fscan(r, v.E[0], v.E[1], v.E[2])
	return err
}

// OStream writes space-separated vector values to a Writer
func (v Vec) OStream(w io.Writer) error {
	_, err := fmt.Fprint(w, v.E[0], v.E[1], v.E[2])
	return err
}

// Plus returns the sum of two vectors
func (v Vec) Plus(v2 Vec) Vec {
	return NewVec(v.E[0]+v2.E[0], v.E[1]+v2.E[1], v.E[2]+v2.E[2])
}

// Minus returns the difference of two vectors
func (v Vec) Minus(v2 Vec) Vec {
	return NewVec(v.E[0]-v2.E[0], v.E[1]-v2.E[1], v.E[2]-v2.E[2])
}

// Times returns the multiplication of two vectors
func (v Vec) Times(v2 Vec) Vec {
	return NewVec(v.E[0]*v2.E[0], v.E[1]*v2.E[1], v.E[2]*v2.E[2])
}

// Div returns the division of two vectors
func (v Vec) Div(v2 Vec) Vec {
	return NewVec(v.E[0]/v2.E[0], v.E[1]/v2.E[1], v.E[2]/v2.E[2])
}

// Scaled returns a vector scaled by a scalar
func (v Vec) Scaled(n float64) Vec {
	return NewVec(v.E[0]*n, v.E[1]*n, v.E[2]*n)
}

// Dot returns the dot product of two vectors
func (v Vec) Dot(v2 Vec) float64 {
	return v.E[0]*v2.E[0] + v.E[1]*v2.E[1] + v.E[2]*v2.E[2]
}

// Cross returns the cross product of two vectors
func (v Vec) Cross(v2 Vec) Vec {
	return NewVec(
		v.E[1]*v2.E[2]-v.E[2]*v2.E[1],
		v.E[2]*v2.E[0]-v.E[0]*v2.E[2],
		v.E[0]*v2.E[1]-v.E[1]*v2.E[0],
	)
}

// GammaCorrect returns a gamma corrected color value
func (c Vec) GammaCorrect(gamma float64) Vec {
	r := math.Pow(c.R(), 1.0/gamma)
	g := math.Pow(c.G(), 1.0/gamma)
	b := math.Pow(c.B(), 1.0/gamma)
	return NewVec(r, g, b)
}
