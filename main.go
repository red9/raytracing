package main

import (
	"fmt"
	"gitlab.com/red9/raytracing/geom"
	"gitlab.com/red9/raytracing/img"
	"gitlab.com/red9/raytracing/rnd"
	"math"
	"math/rand"
	"os"
	"sync/atomic"
	"time"
)

var start time.Time = time.Now()

func main() {
	rand.Seed(start.Unix())
	nx := 700
	ny := 400
	ns := 40
	orbAmount := 40
	updateDelay := 2500
	outfile, _ := os.Create(os.Args[1])
	outfile.WriteString(fmt.Sprintf("P3\n%d %d\n255\n", nx, ny))
	cam := img.NewCamera(
		0.8, float64(nx)/float64(ny),
		geom.NewVec(0, 1, 2),
		geom.NewVec(0, 0, -1),
		0.0001,
		2.5,
	)
	world := geom.List{make([]geom.Hittable, orbAmount)}
	world.List[0] = geom.Sphere{
		geom.NewVec(0, -500, -1), 500,
		&geom.Metal{geom.NewVec(0.4, 0.6, 0.6), 0.05},
	}
	world.List[1] = geom.Sphere{
		geom.NewVec(1, 0.8, -2), 0.8,
		&geom.Dielectric{geom.NewVec(0.8, 0.8, 0.8), 2.5},
	}
	world.List[2] = geom.Sphere{
		geom.NewVec(0, 1, -4), 1,
		&geom.Metal{geom.NewVec(0.5, 0.5, 0.8), 0.3},
	}
	for i := 3; i < orbAmount; i++ {
		world.List[i] = generateRandomObject(geom.NewVec(0, 0, -1), 2, 0.0, 0.15)
	}
	outArr := make([]string, ny)
	randSrc := rnd.Init(5000)
	var done uint32 = 0
	for j := ny - 1; j >= 0; j-- {
		line := func(y int, outPtr *string) {
			randSrc = randSrc.Copy()
			outStr := ""
			for i := 0; i < nx; i++ {
				atomic.AddUint32(&done, 1)
				col := geom.NewVec(0, 0, 0)
				for s := 0; s < ns; s++ {
					u := (float64(i) + randSrc.Next()) / float64(nx)
					v := (float64(y) + randSrc.Next()) / float64(ny)
					r := cam.GetRay(u, v, randSrc)
					col = col.Plus(colour(r, world, 0, randSrc))
				}
				col = col.Scaled(1 / float64(ns))
				col = col.GammaCorrect(1.8)
				ir := int(255.99 * col.R())
				ig := int(255.99 * col.G())
				ib := int(255.99 * col.B())
				if ir < 0 || ig < 0 || ib < 0 || ir > 255 || ig > 255 || ib > 255 {
					outStr += fmt.Sprint("0 0 0\n")
				} else {
					outStr += fmt.Sprintf("%d %d %d\n", ir, ig, ib)
				}
			}
			*outPtr = outStr
		}
		go line(j, &outArr[ny-j-1])
	}
	for i := 0; int(done) < ny*nx; i++ {
		if i > (updateDelay / 50) {
			logTime(1, float64(done)/float64(ny*nx))
			i = 0
		}
		time.Sleep(time.Millisecond * 50)
	}
	for _, v := range outArr {
		outfile.WriteString(v)
	}
	logTime(1, 1)
}

func colour(r geom.Ray, world geom.Hittable, depth int, rnd *rnd.RandSrc) geom.Vec {
	var rec geom.HitRecord
	if world.Hit(r, 0.001, math.MaxFloat64, &rec) {
		var scattered geom.Ray
		var attenuation geom.Vec
		if depth < 30 && rec.Mat.Scatter(&r, &rec, &attenuation, &scattered, rnd) {
			return attenuation.Times(colour(scattered, world, depth+1, rnd))
		} else {
			return geom.NewVec(0, 0, 0)
		}
	} else {
		unit_d := r.Direction.ToUnit()
		t := (unit_d.Y() + 1.0) * 0.5
		return geom.NewVec(1, 1, 1).Scaled(1 - t).Plus(geom.NewVec(0.5, 0.7, 1.0).Scaled(t))
	}
}

func logTime(precision int, completion float64) {
	fmt.Printf("%.*f%% complete in %.1f s\n", precision, completion*100, time.Since(start).Seconds())
}

func generateRandomObject(origin geom.Vec, maxDist float64, maxDistVertical float64, radius float64) geom.Hittable {
	x := origin.X() + (rand.Float64()-0.5)*2*maxDist
	y := origin.Y() + radius + (rand.Float64())*maxDistVertical
	z := origin.Z() + (rand.Float64()-0.5)*2*maxDist
	center := geom.NewVec(x, y, z)
	t := rand.Float64()
	switch {
	case t < 0.3:
		colour := geom.NewVec(0.78+rand.Float64()*0.2, 0.78+rand.Float64()*0.2, 0.78+rand.Float64()*0.2)
		mat := geom.Dielectric{colour, 1.7}
		return geom.Sphere{center, radius, mat}
	case t > 0.6:
		colour := geom.NewVec(0.3+rand.Float64()*0.6, 0.3+rand.Float64()*0.6, 0.3+rand.Float64()*0.6)
		mat := geom.Metal{colour, 0.1}
		return geom.Sphere{center, radius, mat}
	default:
		colour := geom.NewVec(0.3+rand.Float64()*0.6, 0.3+rand.Float64()*0.6, 0.3+rand.Float64()*0.6)
		mat := geom.Matte{colour}
		return geom.Sphere{center, radius, mat}
	}
}
