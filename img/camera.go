package img

import (
	"gitlab.com/red9/raytracing/geom"
	"gitlab.com/red9/raytracing/rnd"
	"math"
)

type Camera struct {
	Vfov        float64
	Hfov        float64
	Horizontal  geom.Vec
	Vertical    geom.Vec
	LensRadius  float64
	FocusLength float64
	geom.Ray
}

func (c Camera) GetRay(u float64, v float64, rnd *rnd.RandSrc) geom.Ray {
	origin := c.Origin

	//Rotate the direction ray by an angle in both the horizontal and vertical axes.
	vTheta := (v - 0.5) * (c.Vfov)
	hTheta := (u - 0.5) * (c.Hfov)
	dVertical := c.Vertical.Scaled(math.Tan(vTheta) * c.FocusLength)
	dHorizontal := c.Horizontal.Scaled(math.Tan(hTheta) * c.FocusLength)
	focusPoint := origin.Plus(c.Direction.Scaled(c.FocusLength).Plus(dVertical).Plus(dHorizontal))
	origin = origin.Plus(geom.RandomInUnitCircle(c.Vertical, c.Horizontal, rnd).Scaled(c.LensRadius))
	return geom.Ray{
		origin,
		focusPoint.Minus(origin),
	}
}

// creates a new camera at origin looking at a point lookAt, with a vertical FoV equal to vfov (radians)
// with aspect ratio equal to aspect (horizontal/vertical)
func NewCamera(
	vfov float64,
	aspect float64,
	origin geom.Vec,
	lookAt geom.Vec,
	lensRadius float64,
	focusLength float64,
) Camera {
	a := lookAt.Minus(origin).ToUnit() //the vector from the location of the camera, to the point being looked at

	// here we take the projection of a base "up" vector on the plane perpendicular
	// to the direction we're looking at to acquire the vertical vector
	// TODO: this breaks with cameras looking straight up or straight down.
	baseUp := geom.NewVec(0, 1, 0)
	vertical := baseUp.Minus(baseUp.Scaled(baseUp.Dot(a))).ToUnit()
	//and do the same for horizontal
	// TODO: this breaks with cameras looking straight right or straight left.
	baseRight := geom.NewVec(1, 0, 0)
	horizontal := baseRight.Minus(baseRight.Scaled(baseRight.Dot(a))).ToUnit()

	camera := Camera{
		vfov, vfov * aspect, horizontal, vertical,
		lensRadius, focusLength,
		geom.Ray{origin, a},
	}
	return camera
}
