package rnd

import (
	"math/rand"
)

type RandSrc struct {
	rands []float64
	i     int
	size  int
}

//Initializes the random number list
func Init(acc int) *RandSrc {
	rands := make([]float64, acc+50)
	for i := range rands {
		rands[i] = rand.Float64()
	}
	r := RandSrc{rands, 0, acc}
	return &r
}

//Initializes the random number list with an existing random source as base
func (r *RandSrc) Copy() *RandSrc {
	return &RandSrc{r.rands, r.i, r.size}
}

//Returns the next random float
func (r *RandSrc) Next() float64 {
	next := r.rands[r.i]
	r.i++
	if r.i >= r.size {
		r.i = rand.Intn(r.size)
	}
	return next
}
